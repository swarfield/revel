defmodule Revel.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    opts = [strategy: :one_for_one, name: Revel.Supervisor]

    # Starts config first so Revel.Router can pull address/port
    children = [
      Revel.Config,
      Revel.Database,
      Revel.Converter,
      Revel.Router
    ]

    Supervisor.start_link(children, opts)
  end
end
