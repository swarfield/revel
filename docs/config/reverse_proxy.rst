Reverse Proxy
=============

Using a reverse proxy is helpful for adding Revel to an existing server.

This can allow you to use your existing to manage subdomains, SSL/TLS,
and other configuration.

Variables
---------

This tutorial will use a few variables throughout. Fill them in as you go
with your information.


- ``<server_ip>``: This is the IP of your server 

  - If you are using docker, this is the ip we found in the docker installation tutorial.
  - If you are running from source, this defaults to ``127.0.0.1``.



NGINX Reverse Proxy
-------------------

Example configuration block for NGINX::

    location ^~ /revel/ {
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto http;
            proxy_set_header Host $http_host;
            proxy_max_temp_file_size 0;
            proxy_pass http://<server_ip>:32320/;
            proxy_redirect http:// https://;
    }

