/*
 *
 *   This file is part of Revel.
 *
 *   Revel is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

/* 
 * This file is adapted from https://github.com/davidgfnet/supersonic-cpp/.
 * Thanks and credits to David G.F.
 */

#include "indexer.h"

using namespace std;

uint64_t calculate_id(const char *string)
{
    uint64_t hash = 14695981039346656037ULL;
    for (int i = 0; ; i++) {
        if(!string[i]) {
            break;
        }
        hash ^= string[i];
        hash *= 1099511628211ULL;
    } 

    // Sqlite doesn't like unsigned numbers :D
    return hash & 0x7FFFFFFFFFFFFFFF;
}

int scan_func(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
    if (ftwbuf->level == 0) {
        return 0;
    }
    if (sb->st_ctim.tv_sec > last_scanned) {
        total_scanned++;

        index_element element;
        element.id=calculate_id(fpath);
        element.parent_id = ids[ftwbuf->level - 1];
        strlcpy(element.path, fpath, 4096);
        element.name_offset = ftwbuf->base;

        if (typeflag == FTW_F) {
            element.folder = false;
        }
        else if (typeflag == FTW_D) {
            element.folder = true;
            //last_folder_id = calculate_id(fpath);
            ids[ftwbuf->level] = calculate_id(fpath);
        }

        to_add.push(element);
    }
    else {
        if (typeflag == FTW_D) {
            ids[ftwbuf->level] = calculate_id(fpath);
        }
    }

    return 0;
}


void insert_song(sqlite3* sqldb,
                 uint64_t id,
                 string   title,
                 string   artist,
                 unsigned trackn,
                 unsigned year,
                 unsigned duration,
                 string genre,
                 const char *ext,
                 const char *filename,
                 uint64_t parent_id,
                 const char *filepath)
{
    
    sqlite3_stmt *insert_file_into_files;
    sqlite3_stmt *insert_into_entities;
    
    sqlite3_prepare_v2(sqldb, insert_into_files_string, -1, &insert_file_into_files, NULL);
 	sqlite3_prepare_v2(sqldb, insert_into_entities_string, -1, &insert_into_entities, NULL);

	sqlite3_bind_int64(insert_file_into_files, 1, id);
	sqlite3_bind_text(insert_file_into_files, 2, title.c_str(), -1, NULL);
    sqlite3_bind_text(insert_file_into_files, 3, artist.c_str(), -1, NULL);
    sqlite3_bind_int(insert_file_into_files, 4, trackn);
    sqlite3_bind_int(insert_file_into_files, 5, year);
    sqlite3_bind_int(insert_file_into_files, 6, duration);
    sqlite3_bind_text(insert_file_into_files, 7, genre.c_str(), -1, NULL);
    sqlite3_bind_text(insert_file_into_files, 8, ext, -1, NULL);
    sqlite3_bind_text(insert_file_into_files, 9, filename, -1, NULL);

	if (sqlite3_step(insert_file_into_files) != SQLITE_DONE) {
	    cerr << "failed to insert file: " << sqlite3_errmsg(sqldb) << endl;
	}
        	
	sqlite3_bind_int64(insert_into_entities, 1, id);
	sqlite3_bind_int64(insert_into_entities, 2, parent_id);
    sqlite3_bind_text(insert_into_entities, 3, "file", -1, NULL);
	sqlite3_bind_text(insert_into_entities, 4, filepath, -1, NULL);
	if (sqlite3_step(insert_into_entities) != SQLITE_DONE) {
	    cerr << "failed to insert file: " << sqlite3_errmsg(sqldb) << endl;
	}
} 

bool scan_music_file(int id, sqlite3 *sqldb, const index_element file)
{

    string  artist;
    string  albumartist;
    string  album;
    string  title;

    char *ext = strrchr((char*)file.path, '.');
    if (!ext) return false;
    ext++;

    if (strcasecmp(ext, "mp3") && strcasecmp(ext, "ogg") && strcasecmp(ext, "flac") && strcasecmp(ext, "m4a")) {
        return false;
    }

    TagLib::FileRef f(file.path);
    if (f.isNull())
        return false;

    TagLib::Tag *tag = f.tag();
    if (!tag)
        return false;


    string cover;
    if (!strcasecmp(ext, "mp3")) {
        TagLib::MPEG::File audioFile(file.path);
        TagLib::ID3v2::Tag *mp3_tag = audioFile.ID3v2Tag(true);

        if (mp3_tag) {
            // get album art
            auto pic_frames = mp3_tag->frameList("APIC");
            if (!pic_frames.isEmpty()) {
                auto frame  = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(pic_frames.front());
                cover       = string(frame->picture().data(), frame->picture().size());
            }
            // get album artist
            auto tpe2_frame = mp3_tag->frameList("TPE2");
            if (!tpe2_frame.isEmpty()) {
                albumartist = tpe2_frame.front()->toString().toCString(true);
            }
        }
    }
    else if (!strcasecmp(ext, "ogg")) {
        auto vorbis_tag = dynamic_cast<TagLib::Ogg::XiphComment *>(tag);
        if (vorbis_tag) {
            // get album artist
            if (vorbis_tag->properties().contains("ALBUMARTIST")) {
                albumartist = vorbis_tag->properties()["ALBUMARTIST"][0].toCString(true);
            }
        }
    } else if (!strcasecmp(ext, "flac")) {
        auto flac_tag   = dynamic_cast< TagLib::FLAC::File *>(tag);
        if (flac_tag) {
            TagLib::List<TagLib::FLAC::Picture*> picList = flac_tag->pictureList();
            if (picList.size() > 0) {
                cover = string(picList[0]->data().data(), picList[0]->data().size());
            }
        }
        auto prop  = tag->properties();
        if (!prop.isEmpty()) {
            // get album artist
            if (prop.contains("ALBUMARTIST")) {
                albumartist = prop["ALBUMARTIST"][0].toCString(true);
            }
        }
    } else if (!strcasecmp(ext, "m4a")) {
        auto m4a_tag   = dynamic_cast< TagLib::MP4::Tag *>(tag);
        if (m4a_tag) {
            auto map = m4a_tag->itemListMap();
            // get cover art
            if (map.contains("covr")) {
                TagLib::MP4::CoverArtList picList = map["covr"].toCoverArtList();
                if (picList.size() > 0) {
                    cover = string(picList[0].data().data(), picList[0].data().size());
                }
            }
            // get album artist
            if (map.contains("aART") && map["aART"].toStringList().size() > 0) {
                albumartist = map["aART"].toStringList()[0].toCString(true);
            }
        }
    }

    // if we've found an albumartist tag, use that to index the song
    if (albumartist.size() != 0) {
        artist = albumartist;
    } else {
        artist  = tag->artist().toCString(true);
    }
    album   = tag->album().toCString(true);
    title   = tag->title().toCString(true);


    TagLib::AudioProperties *properties = f.audioProperties();
    if (!properties) {
        cout << "ignored " << file.path << ": no audio metadata present" << endl;
        return false;
    }

    // if the file didn't have any cover art tag but the directory scanner found
    // one, use that to populate the db. Reject files bigger than 400kB.
    /*
    if (cover.size() == 0 && dir_cover.size() > 0 && dir_cover.size() < 400 * 1024) {
        cover = dir_cover;
    }
    */
    
    insert_song(sqldb,
                file.id,
                title,
                artist,
                tag->track(),
                tag->year(),
                properties->length(),
                tag->genre().toCString(true),
                ext,
                file.path + file.name_offset,
                file.parent_id,
                file.path
            );
    
    return true;
}

void insert_folder(int id, sqlite3 *sqldb, const index_element folder)
{
    sqlite3_stmt *insert_folder_into_folders;
    sqlite3_stmt *insert_into_entities;
    
    sqlite3_prepare_v2(sqldb, insert_into_folders_string, -1, &insert_folder_into_folders, NULL);
 	sqlite3_prepare_v2(sqldb, insert_into_entities_string, -1, &insert_into_entities, NULL);

	sqlite3_bind_int64(insert_folder_into_folders, 1, folder.id);
	sqlite3_bind_text(insert_folder_into_folders, 2, folder.path + folder.name_offset, -1, NULL);

	if (sqlite3_step(insert_folder_into_folders) != SQLITE_DONE) {
	    cerr << "failed to insert folder: " << sqlite3_errmsg(sqldb) << endl;
	}

	sqlite3_bind_int64(insert_into_entities, 1, folder.id);
	sqlite3_bind_int64(insert_into_entities, 2, folder.parent_id);
    sqlite3_bind_text(insert_into_entities, 3, "folder", -1, NULL);
    sqlite3_bind_text(insert_into_entities, 4, folder.path, -1, NULL);
    if (sqlite3_step(insert_into_entities) != SQLITE_DONE) {
	    cerr << "failed to insert folder: " << sqlite3_errmsg(sqldb) << endl;
	}
}

inline bool file_exists(const char *name)
{
    struct stat buffer; 
    return (stat(name, &buffer) == 0); 
}

int main(int argc, char* argv[])
{
    sqlite3*    sqldb;

    if (argc < 2) {
        return -1;
    }

    char *db_file = argv[1];
    char *folder_to_scan = argv[2];
    
    bool db_exists = file_exists(db_file);

    sqlite3_open_v2(db_file, &sqldb, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
    sqlite3_busy_timeout(sqldb, 5000);
 
    if (!db_exists) {
        // if the database was just created run the init_sql query
        sqlite3_exec(sqldb, init_sql, NULL, NULL, NULL);
    }

    sqlite3_stmt *get_timestamp;
    sqlite3_prepare_v2(sqldb, get_timestamp_string, -1, &get_timestamp, NULL);

    sqlite3_step(get_timestamp);
    last_scanned = sqlite3_column_int64(get_timestamp, 0);
    
    if (sqlite3_step(get_timestamp) != SQLITE_DONE) {
	    cerr << "cannot retrieve timestamp from database: " << sqlite3_errmsg(sqldb) << endl;
	}

    sqlite3_stmt *update_timestamp;
    sqlite3_prepare_v2(sqldb, update_timestamp_string,  -1, &update_timestamp, NULL);
    sqlite3_bind_int64(update_timestamp, 1, (unsigned long)time(NULL));

    if (sqlite3_step(update_timestamp) != SQLITE_DONE) {
	    cerr << "failed to update timestamp: " << sqlite3_errmsg(sqldb) << endl;
	}

    // put all files and folders into a queue to be scanned
    nftw(folder_to_scan, scan_func, 50, FTW_PHYS);

    printf("total_scanned: %d\n", total_scanned);

    ctpl::thread_pool p(5);



    // open a transaction
    sqlite3_exec(sqldb, "BEGIN TRANSACTION;", NULL, NULL, NULL);    

    while (!to_add.empty()) {
        index_element element = to_add.front();
        to_add.pop();
        if (element.folder) {
            p.push(insert_folder, sqldb, element); 
        }
        else {
            p.push(scan_music_file, sqldb, element);
        }
    }
    // end transaction
    sqlite3_exec(sqldb, "COMMIT;", NULL, NULL, NULL);
    
    sqlite3_close(sqldb);

    return 0;
}
