/*
 *
 *   This file is part of Revel.
 *
 *   Revel is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

/* 
 * This file is adapted from https://github.com/davidgfnet/supersonic-cpp/.
 * Thanks and credits to David G.F.
 */

#include <string>
#include <queue>

#include <stdio.h>
#include <stdint.h>
#include <bsd/string.h>
#include <ftw.h>
#include <time.h>

#include <sqlite3.h> 

#include <taglib/fileref.h> 
#include <taglib/tag.h> 
#include <taglib/tpropertymap.h> 
#include <taglib/mpegfile.h> 
#include <taglib/id3v2tag.h> 
#include <taglib/oggfile.h> 
#include <taglib/vorbisfile.h> 
#include <taglib/flacfile.h> 
#include <taglib/mp4file.h> 
#include <taglib/attachedpictureframe.h> 
#include <taglib/flacpicture.h> 

#include "ctpl_stl.h"

const char *init_sql = "\
    CREATE TABLE entities (\
        id        INTEGER NOT NULL UNIQUE,\
        parent    INTEGER NOT NULL,\
        type      TEXT,\
        filepath  TEXT,\
        PRIMARY KEY(id)\
    );\
    CREATE TABLE files (\
        id        INTEGER NOT NULL UNIQUE,\
        title     TEXT,\
        artist    TEXT,\
        trackn    INTEGER NOT NULL,\
        year      INTEGER,\
        duration  INTEGER,\
        genre     TEXT,\
        ext       TEXT,\
        filename  TEXT,\
        PRIMARY KEY(id)\
    );\
    CREATE TABLE folders (\
        id        INTEGER NOT NULL UNIQUE,\
        filename  TEXT,\
        PRIMARY KEY(id)\
    );\
    CREATE TABLE users (\
        username         TEXT NOT NULL UNIQUE,\
        \"password\"     TEXT,\
        admin_role       INTEGER,\
        settings_role    INTEGER,\
        download_role    INTEGER,\
        playlist_role    INTEGER,\
        cover_art_role  INTEGER,\
        PRIMARY KEY(username)\
    );\
    INSERT INTO users (username, \"password\", admin_role, settings_role, download_role, playlist_role, cover_art_role) VALUES ('admin', \"password\", 1, 1, 1, 1, 1);\
    CREATE TABLE roots (\
        name      TEXT UNIQUE,\
        entity_id INTEGER NOT NULL UNIQUE\
    );\
    CREATE TABLE metadata (\
        keyword   TEXT UNIQUE,\
        val       INTEGER\
    );\
    INSERT INTO metadata (keyword, val) VALUES ('last_scanned', 0);\
";

const char *update_timestamp_string = "INSERT OR REPLACE INTO metadata VALUES ('last_scanned', ?);";
const char *get_timestamp_string = "SELECT val FROM metadata WHERE keyword like 'last_scanned'";

const char *insert_into_folders_string = "INSERT OR REPLACE INTO `folders` (`id`, `filename`) VALUES (?,?);";
const char *insert_into_files_string = "INSERT OR REPLACE INTO `files` ("
                 "`id`, `title`, `artist`, `trackn`, `year`, `duration`, `genre`, `ext`, `filename`)"
                 "VALUES (?,?,?,?,?,?,?,?,?);";
const char *insert_into_entities_string = "INSERT OR REPLACE INTO `entities` (`id`, `parent`, `type`, `filepath`) VALUES (?,?,?,?);";

struct index_element {
    char path[4096];
    int name_offset;
    bool folder;
    uint64_t id;
    uint64_t parent_id;
};

// ids, to_add, last_scanned, and total_scanned must be global beacuse nftw doesn't take a void*
uint64_t ids[128] = {0};
std::queue<index_element> to_add;
uint64_t last_scanned;
uint64_t total_scanned = 0;

uint64_t calculate_id(const char *string);
int scan_func(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf);
void insert_song(sqlite3* sqldb, uint64_t id, std::string title, std::string artist, unsigned trackn,
        unsigned year, unsigned duration, std::string genre, const char *ext, const char *filename,
        uint64_t parentid, const char *filepath);
bool scan_music_file(int id, sqlite3 *sqldb, const index_element file);
void insert_folder(int id, sqlite3 *sqldb, const index_element folder);
